const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
var app = require('./app');
const saltRounds = 10;

module.exports = {
    generateSalt : function(callback) {
        bcrypt.genSalt(saltRounds,function(err,salt){
            if(err) {
                    callback(err,null)
            }
            else{
                    callback(null,salt)
            }
                
        })
    },

    generateHash : function(password,salt,callback){
        bcrypt.hash(password,salt,function(err,hash){
            if(err) {
                callback(err,null)
            }
            else{
                callback(null,hash)
            }
        })
    },

    generateToken: function(data,expire,callback){
        jwt.sign({data:data},'rana',{ expiresIn : expire*60*60 },function(err,res){
                if(err) {
                    callback(err,null)
                }
                else {
                    callback(null,res)
                }
        })
    },
    
    isauth : function(req,res,callback) {
        jwt.verify(req.body.token, 'rana', function(err, decoded) {
            if(err) {
                res.json({
                    "reason":"jwt token invalid"
                })
            }
            else {
                callback()
            }
          });          
    },

    decode:function(token,callback) {
        jwt.verify(token, 'rana', function(err, decoded) {
            if(err) {
                callback(err,null)
            }
            else {
                callback(null,decoded)
            }
          }); 
    }
}