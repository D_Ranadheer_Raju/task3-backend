var express = require("express");
var cors = require('cors');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var middleware = require('./middleware');
var app = express();
var port = 5000;
var puretext = require('puretext');
const CRYPTR = require('cryptr');       //importing the encryption lib
const cryptr = new CRYPTR('venky');
const bcrypt = require('bcrypt');
const saltRounds = 10;  //it is used to encrypt the password  like key
app.use(bodyParser.json());

var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'ranadheerraju11@gmail.com',
        pass: 'r@n@dheerr@ju11'
    }
});

var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/test");
var nameSchema = new mongoose.Schema({
    email: String,
    password: String,
    salt: String,
    firstname: String,
    lastname: String,
    address: String,
    isVerified: Boolean
});
var Task3 = mongoose.model("Task3", nameSchema, "Task3");

app.use(cors());

app.get("/", (req, res) => {
    res.send("hello");
});

app.post("/api/register", (req, res) => {
    var dbo = Task3.db;
    // rand=Math.floor((Math.random() * 100) + 54);
    data = {
        email: req.body.email
    }
    middleware.generateToken(data, 23, function (err, token) {
        if (err) throw err;
        //console.log(token)
        host = req.get('host');
        link = "http://" + req.get('host') + "/verify?id=" + token;
        var mailOptions = {
            from: 'ranadheerraju11@gmail.com',
            to: req.body.email,
            subject: 'Please verify your email',
            html: "Hello,<br> Please Click on the link to verify your email.<br><a href=" + link + ">Click here to verify</a>"
        };
        Task3.findOne({ email: req.body.email }, function (err, result) {
            if (!result) {
                transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        console.log('Generating Email error: ',error);
                    } else {
                        console.log('Email sent: ' + info.response);
                    }
                });
                bcrypt.genSalt(saltRounds, function (err, salt) {
                    bcrypt.hash(req.body.password, salt, function (err, hash) {
                        // Store hash in your password DB.
                        var myobj = { email: req.body.email, password: hash, salt: salt, isVerified: req.body.isVerified };
                        dbo.collection("Task3").insertOne(myobj, function (err, docs) {
                            if (err) throw err;
                            res.json({
                                "status": true
                            })
                        });
                    });
                });

            } else {
                res.json({
                    "status": false
                })
            }
        });

    });
});
app.get('/verify', function (req, res) {
    var isVerifiedu = true;
    // link="http://"+req.get('host')+"/api/profile";
    console.log("Token Id:", req.query.id);
    // console.log(req.protocol+":/"+req.get('host'));
    // console.log("Host 9876543456789:",req.get('host'))
    if ((req.protocol + "://" + req.get('host')) == ("http://" + host)) {
        console.log("Domain is matched. Information is from Authentic email");
        middleware.decode(req.query.id, function (err, decode) {
            console.log("YES", decode)
            console.log(req.query.id)
            if (decode) {
                Task3.findOne({ "email": decode.data.email }, function (err, docs) {
                    if (!docs) {
                        res.end(
                            "<html><b>Sorry, we didn't found any account on this email\n\nYou want to register again please</b><a href='http://192.168.1.12:3000/api/register'>'Click here'</a></html>"
                        );
                    }
                    else {
                        console.log("suceess", decode.data.email);
                        Task3.findOneAndUpdate(decode.data.email, { "$set": { isVerified: true } }, function (err, update) {

                            if (!update) {
                                console.log("Not updated", err);
                            } else {
                                console.log("email is verified");
                                res.end("<a href='http://192.168.1.66:3000/api/mobile'>'Click here to verify your email and mobile'</a>");
                            }
                        })
                    }
                })
            }
            else {
                res.end("<h1>Request is from unknown source");
            }
        })
    }
    else {
        res.end("expired")
    }
});
app.post('/api/login', function (req, res) {
    var email = req.body.email;
    console.log(email)
    Task3.findOne({ email: req.body.email }, function (err, docs) {
        if (!docs) {
            res.json({
                "status": false
            })
        } else {
            if (err) throw err;
            var pass = docs.password;
            // console.log(docs)
            bcrypt.compare(req.body.password, docs.password, async function (err, views) {
                if (err) throw err;
                if (docs.isVerified === false) {
                    res.json({
                        "status": true
                    })
                } else {
                    if (views === true) {
                        data = {
                            email: req.body.email,
                            id: docs._id
                        }
                        //console.log(data)
                        middleware.generateToken(data, 23, function (err, token) {
                            if (err) {
                                res.json({
                                    "status": "ok",
                                    "response": "Error while generating token"
                                })
                            }
                            else {
                                res.json({
                                    "status": "ok",
                                    "success": true,
                                    "email": req.body.email,
                                    "token": token,
                                    "isVerified": docs.isVerified
                                })
                            }
                        })
                    } else {
                        res.send("Invalid credentials");
                    }
                }
            });
        }
    });
});

app.put('/api/profile', function (req, res) {
    var updateData = { $set: { firstname: req.body.firstname, lastname: req.body.lastname, address: req.body.address } };
    //console.log(req.body.token)
    middleware.decode(req.body.token, function (err, decode) {
        // console.log("token:" + req.body.token + "\n decode id:" + decode.data.id)
        if (decode) {
            Task3.findByIdAndUpdate(decode.data.id, updateData, function (err, update) {

                if (err) {
                    res.json({
                        "reason": "error while updating data"
                    })
                }
                else {
                    res.json({
                        "success": true,
                        "message": "successfuly updated messages",
                        "firstname": req.body.firstname,
                        "lastname": req.body.lastname,
                        "address": req.body.address
                    })
                }
            })
        }
    })
});

app.delete('/api/delete', function (req, res) {
    console.log(req.body.token)
    middleware.decode(req.body.token, function (err, decode) {
        console.log("YES", decode)
        if (decode) {
            Task3.findOneAndRemove({ "_id": decode.data.id }, function (err, deletes) {
                if (!deletes) {
                    res.json({
                        "reason": "error while deleting",
                        "error": err,
                        "status": false
                    })
                }
                else {
                    res.json({
                        "status": true,
                        "message": "successfuly deleted",
                        "resp": deletes
                    })
                }
            })
        }
    })
    // Task3.deleteOne({ email: req.body.email }, function (err, docs) {
    //     if (err) throw err;
    //     res.send("Deleted");
    // });
})

app.post('/api/mobile', function (req, res) {
    console.log(req.body.mobile);

    var otp = Math.floor(100000 + Math.random() * 900000);

    let text = {
        // To Number is the number you will be sending the text to.
        toNumber: req.body.mobile,
        // From number is the number you will buy from your admin dashboard
        fromNumber: '+13152845929',
        // Text Content
        smsBody: `Hi user this is your admin and your otp is ${otp}`,
        //Sign up for an account to get an API Token
        apiToken: 'ahmn52'
    }
    puretext.send(text, function (err, response) {
        if (err) {
            console.log(err);
        } else {
            console.log(otp);
            res.json({
                "status": true,
                "otp": otp
            });
        }
    })
});
app.listen(port, () => {
    console.log("Server listening on port " + port);
});
